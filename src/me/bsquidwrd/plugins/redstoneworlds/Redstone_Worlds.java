package me.bsquidwrd.plugins.redstoneworlds;

import java.io.File;

import me.bsquidwrd.plugins.redstoneworlds.commands.RW;
import me.bsquidwrd.plugins.redstoneworlds.commands.RWReload;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Redstone_Worlds extends JavaPlugin {
	
	public static File pluginDir;
	public static FileConfiguration config;
	public static JavaPlugin plugin;
	
	public Functions funcs = new Functions();

	public void onDisable() {

	}

	public void onEnable() {
		plugin = this;
		this.getServer().getPluginManager().registerEvents(new Redstone_Listener(), this);
		pluginDir = this.getDataFolder();
		this.getCommand("rw").setExecutor(new RW(this));
		this.getCommand("rwreload").setExecutor(new RWReload(this));
		saveDefaultConfig();
		Redstone_Worlds.config = getConfig();
		Functions.waitingForConfirm = false;
		Functions.confirmFor = 0;
	}

}
