package me.bsquidwrd.plugins.redstoneworlds.commands;

import me.bsquidwrd.plugins.redstoneworlds.Functions;
import me.bsquidwrd.plugins.redstoneworlds.Redstone_Worlds;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RW implements CommandExecutor {

	private Redstone_Worlds plugin;
	public Functions funcs = new Functions();

	public RW(Redstone_Worlds plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 0) {
			sender.sendMessage(ChatColor.AQUA + "----------" + ChatColor.GRAY + "[" + ChatColor.RED + "Redstone Worlds" + ChatColor.GRAY + "]" + ChatColor.AQUA + "----------");
			sender.sendMessage(ChatColor.AQUA + "Created by: " + ChatColor.RED + "bsquidwrd");
			sender.sendMessage(ChatColor.AQUA + "Version: " + ChatColor.RED + plugin.getDescription().getVersion());
			if(sender instanceof Player) {
				funcs.confirmFor(1);
			}
			return true;
		} else if(args.length == 1 && args[0].equalsIgnoreCase("confirm")) {
			if(sender instanceof Player) {
				Player player = (Player)sender;
				int id = Functions.confirmFor;
				switch (id) {
				case 1:
					funcs.deleteWorld(player, false);
					break;

				default:
					player.sendMessage("You are not waitingto confirm anything");
					break;
				}

				return true;
			} else {
				sender.sendMessage(ChatColor.RED + "[RedstoneWorlds] You must be a player to use that command!");
				return true;
			}
		}
		return false;
	}
	
	

}
