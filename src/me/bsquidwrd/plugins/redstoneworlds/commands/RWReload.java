package me.bsquidwrd.plugins.redstoneworlds.commands;

import me.bsquidwrd.plugins.redstoneworlds.Functions;
import me.bsquidwrd.plugins.redstoneworlds.Redstone_Worlds;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class RWReload implements CommandExecutor {

	private Redstone_Worlds plugin;
	public Functions funcs = new Functions();
	public FileConfiguration config;
	public Server server = Bukkit.getServer();
	public ConsoleCommandSender console = server.getConsoleSender();

	public RWReload(Redstone_Worlds plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		this.config = Redstone_Worlds.config;
		if(args.length == 0) {
			if(sender.hasPermission("redstoneworlds.permsreload")) {
				for(Player cPlayer: plugin.getServer().getOnlinePlayers()) {
					if(!funcs.hasPerm(cPlayer, "reloadexempt")) {
						String permDeleteCmd = config.getString("permDeleteCmd");
						permDeleteCmd = permDeleteCmd.replace("%username%", cPlayer.getName());
						server.dispatchCommand(console, permDeleteCmd);
						funcs.addBasicPerms(cPlayer, cPlayer.getName());
					}
				}
			} else {
				try {
					funcs.noPerms((Player) sender);
				} catch (Exception e) {
					funcs.logger.warning("[RedstoneWorlds] Tried to tell someone they didn't have permission for something, when they aren't a player. Who could they be?");
				}
			}
		}
		return false;
	}

}
