package me.bsquidwrd.plugins.redstoneworlds;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class Redstone_Listener implements Listener {

	public Functions funcs = new Functions();

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block eventBlock = event.getClickedBlock();
			if(eventBlock.getType() == Material.SIGN_POST || eventBlock.getType() == Material.WALL_SIGN && eventBlock.getState() instanceof Sign) {
				Sign sign = (Sign) eventBlock.getState();
				if(ChatColor.stripColor(sign.getLine(0)).equalsIgnoreCase("[RW]")) {
					Player player = event.getPlayer();
					if(ChatColor.stripColor(sign.getLine(1)).equalsIgnoreCase("Create World")) {
						event.setCancelled(true);
						if(funcs.hasPerm(player, "create")) {
							funcs.setupNewWorld(player);
						} else {
							funcs.noPerms(player);
						}
					} else if(ChatColor.stripColor(sign.getLine(1)).equalsIgnoreCase("Delete World")) {
						event.setCancelled(true);
						if(funcs.hasPerm(player, "delete")) {
							funcs.confirmFor(1);
							player.sendMessage(ChatColor.RED + "WARNING: You are about to reload your world. This will delete everything and create a fresh copy for you from the server");
							player.sendMessage(ChatColor.LIGHT_PURPLE + "If you are sure you would like to continue, you have 10 seconds to type /rw confirm");
							funcs.new RWTask(Redstone_Worlds.plugin).runTaskLater(Redstone_Worlds.plugin, (10 * 20));
						} else {
							funcs.noPerms(player);
						}
					} else if(ChatColor.stripColor(sign.getLine(1)).equalsIgnoreCase("Goto World")) {
						event.setCancelled(true);
						if(funcs.hasPerm(player, "goto")) {
							funcs.gotoWorld(player);
						} else {
							funcs.noPerms(player);
						}
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerPlaceSign(SignChangeEvent event) {
		Block eventBlock = event.getBlock();
		if(eventBlock.getState() instanceof Sign) {
			Player player = event.getPlayer();
			String sLines[] = event.getLines();
			if(sLines[0].equalsIgnoreCase("[RW]")) {
				if(sLines[1].equalsIgnoreCase("create")) {
					if(funcs.hasPerm(player, "signs.create")) {
						event.setLine(0, ChatColor.AQUA + "[RW]");
						event.setLine(1, ChatColor.GREEN + "Create World");
					} else {
						eventBlock.breakNaturally();
						funcs.noPerms(player);
					}
				} else if(sLines[1].equalsIgnoreCase("delete")) {
					if(funcs.hasPerm(player, "signs.reload")) {
						event.setLine(0, ChatColor.AQUA + "[RW]");
						event.setLine(1, ChatColor.GREEN + "Delete World");
					} else {
						eventBlock.breakNaturally();
						funcs.noPerms(player);
					}
				} else if(sLines[1].equalsIgnoreCase("goto")) {
					if(funcs.hasPerm(player, "signs.goto")) {
						event.setLine(0, ChatColor.AQUA + "[RW]");
						event.setLine(1, ChatColor.GREEN + "Goto World");
					} else {
						eventBlock.breakNaturally();
						funcs.noPerms(player);
					}
				}else {
					eventBlock.breakNaturally();
					funcs.pError(player, "[RedstoneWorlds] Please check your sign syntax and try again.");
				}
			}
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Block eventBlock = event.getBlock();
		if(eventBlock.getState() instanceof Sign) {
			Player player = event.getPlayer();
			Sign sign = (Sign) eventBlock.getState();
			String sLines[] = sign.getLines();
			if(ChatColor.stripColor(sLines[0]).equalsIgnoreCase("[RW]")) {
				if(ChatColor.stripColor(sLines[1]).equalsIgnoreCase("Create World")) {
					if(!funcs.hasPerm(player, "signs.create.break")) {
						event.setCancelled(true);
						funcs.pError(player, "You don't have permission to break that sign!");
					}
				} else if(ChatColor.stripColor(sLines[1]).equalsIgnoreCase("Reload World")) {
					if(!funcs.hasPerm(player, "signs.reload.break")) {
						event.setCancelled(true);
						funcs.pError(player, "You don't have permission to break that sign!");
					}
				} else if(ChatColor.stripColor(sLines[1]).equalsIgnoreCase("Goto World")) {
					if(!funcs.hasPerm(player, "signs.goto.break")) {
						event.setCancelled(true);
						funcs.pError(player, "You don't have permission to break that sign!");
					}
				}
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		//Do things later about Player Joins

	}

}
