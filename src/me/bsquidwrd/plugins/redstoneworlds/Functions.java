package me.bsquidwrd.plugins.redstoneworlds;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Functions {

	public Server server = Bukkit.getServer();
	public ConsoleCommandSender console = server.getConsoleSender();
	public Logger logger = server.getLogger();

	public boolean worldLoaded = false;
	public int worldLoadedInt = 0;

	public static boolean waitingForConfirm;
	public static int confirmFor;

	public String basePerm = "redstoneworlds.";

	//Sends an error message to a player
	public void pError(Player player, String message) {
		player.sendMessage(ChatColor.RED + message);
	}

	//Upon world generation, this adds special perms to a user for their world
	public void addBasicPerms(Player player, String worldName) {
		String pName = player.getName();
		String pWorld = pName;
		FileConfiguration config = Redstone_Worlds.config;
		String permAddCmd = config.getString("permAddCmd");
		//String permReloadCmd = config.getString("permReloadCmd");
		List<String> newWorldPerms = config.getStringList("newWorldPerms");
		for(String perm: newWorldPerms) {
			String permCmd = permAddCmd.replace("%username%", pName);
			permCmd = permCmd.replace("%permission%", perm);
			permCmd = permCmd.replace("%world%", pWorld);
			server.dispatchCommand(console, permCmd);
		}
		//server.dispatchCommand(console, permReloadCmd);
	}

	//Generates a random string of characters
	public String generateString(Random rng, String characters, int length) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++)
		{
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	//Tells a player they don't have permissions to do something
	public void noPerms(Player player) {
		pError(player, "You don't have permission to do this.");
	}

	//Sends an error message to any online admins along with logging the error on console
	public void adminError(String message) {
		for(Player cPlayer: server.getOnlinePlayers()) {
			if(cPlayer.hasPermission(basePerm + "admin")) {
				cPlayer.sendMessage(ChatColor.RED + "[RedstoneWorlds] " + ChatColor.GRAY + message);
			} else {
				continue;
			}
		}
		logger.warning("[RedstoneWorlds] " + message);
	}

	//Checks if a player has a specific permission
	public Boolean hasPerm(Player player, String perm) {
		if(player.hasPermission(basePerm + perm)) {
			return true;
		} else {
			return false;
		}
	}

	//Sets up a world from the template for the user to use
	public void setupNewWorld(Player player) {
		String pluginsDirStr = Redstone_Worlds.pluginDir.getParent();

		String mainDirStr = pluginsDirStr + File.separator + ".." + File.separator; //How to get to the main server files
		File mainDir = new File(mainDirStr); //Create a new File object to reference the main folder

		String templateDirStr = pluginsDirStr + File.separator + ".." + File.separator + "template" + File.separator;
		File templateDir = new File(templateDirStr);

		if(mainDir.canRead() && mainDir.canWrite()) { //Can I read/write in the main folder?
			File pWorld = new File(mainDirStr + player.getName());
			List<World> worlds = server.getWorlds();
			if(!worlds.toString().contains(player.getName())) {
				if(pWorld.mkdir()) {
					try {
						//Copies the filese from template to a new folder for a user
						Files.walkFileTree(templateDir.toPath(), new CopyFileVisitor(pWorld.toPath()));
						player.sendMessage(ChatColor.AQUA + "I am creating your world and I will teleport you there when it is ready.");
						String worldName = player.getName();
						//Imports world into MultiVerse with space generator provided by CleanroomGenerator
						String cmdLine = "mv import " + worldName + " normal -g CleanroomGenerator:.";
						server.dispatchCommand(console, cmdLine);
						//This is just a loop to keep checking if the world is loaded
						worldLoaded(worldName);
						//Once the world is finally loaded, make the following tweaks to it before anything else
						if(worldLoaded) {
							World tWorld = server.getWorld(player.getName());
							tWorld.setSpawnLocation(0, 56, 0);
							tWorld.setSpawnFlags(false, false);
							tWorld.setDifficulty(Difficulty.PEACEFUL);
							server.dispatchCommand(console, "mv modify set animals false " + worldName);
							server.dispatchCommand(console, "mv modify set monsters false " + worldName);
							server.dispatchCommand(console, "mv modify set pvp false " + worldName);
							server.dispatchCommand(console, "mv modify set mode creative " + worldName);
							server.dispatchCommand(console, "mv modify set diff 0 " + worldName);
							server.dispatchCommand(console, "mv modify set scale 1.0 " + worldName);
							server.dispatchCommand(console, "wb " + worldName + " set 300 300 0.5 0.5");
							addBasicPerms(player, worldName);
							tpToWorld(player, player.getName());
							server.dispatchCommand(player, "sethome");
							server.dispatchCommand(player, "home");
							player.setBedSpawnLocation(player.getLocation(), true);
							player.sendMessage(ChatColor.GREEN + "Welcome to your very own sandbox! Feel free to build, test, or just look around.");
						} else {
							logger.warning("[RedstoneWorlds] Tried to load a world before it was ready.");
						}
					} catch (IOException e) {
						//Shit just went down and I don't know why, so it logs everything and provides a reference number for easy lookup
						String errorRef = generateString(new Random(), "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5);
						logger.severe("[RedstoneWorlds] Begin Reference: " + errorRef);
						logger.severe("[RedstoneWorlds] " + e.toString());
						logger.severe("[RedstoneWorlds] End Reference: " + errorRef);
						adminError("Umm... Not sure how to tell you this, but something just went wrong, and you better get someone to check console.");
						adminError("Error Reference: " + errorRef);
						pError(player, "Something went wrong with creating your world. Please alert an admin immediately and inform them of this error and to have someone check console.");
						pError(player, "Error Reference: " + errorRef);
					}
				} else {
					//There was an error making a directory for a user. Tell all online admins
					adminError("There was an issue creating a world for " + player.getName() + ". " + player.getName() + "'s world may very well already exist, but is not loaded or a folder already exists with the same name.");
					logger.warning("[RedstoneWorlds] There was an issue creating a world for " + player.getName() + ". " + player.getName() + "'s world may very well already exist, but is not loaded or a folder already exists with the same name.");
				}
			} else {
				//If the player already has a world created and loaded for them, teleport them there
				tpToWorld(player, player.getName());
			}
		} else {
			//I can't read or write to the directory like I need to
			adminError("There is an issue with read/write issue with permissions");
			logger.warning("[RedstoneWorlds] There is an issue with read/write issue with permissions");
		}
	}

	//Deletes a world for a player
	public void deleteWorld(Player player, boolean auto) {
		confirmFor(0);
		String worldName = player.getName();
		FileConfiguration config = Redstone_Worlds.config;
		String worldLoadCmd = config.getString("worldLoadCmd").replace("%world%", worldName);
		server.dispatchCommand(console, worldLoadCmd);
		worldLoaded(worldName);
		if(worldLoaded) {
			String worldDelCmd = config.getString("worldDelCmd");
			worldDelCmd = worldDelCmd.replace("%world%", worldName);
			String worldDelCmdConfirm = config.getString("worldDelCmdConfirm");
			server.dispatchCommand(console, worldDelCmd);
			server.dispatchCommand(console, worldDelCmdConfirm);
			if(!auto) {
				player.sendMessage(ChatColor.GREEN + "I have deleted your world successfully. If you would like to start over, please use a create sign.");
			}
		} else {
			adminError("World '" + worldName + "' is not loaded, so I can not delete it.");
			pError(player, "I am really sorry, but it doesn't appear your world is loaded. Please try to create a world. If that fails, please contact an admin");
		}
	}

	//Teleports a player to their world
	public void gotoWorld(Player player) {
		worldLoaded(player.getName());
		if(worldLoaded){
			tpToWorld(player, player.getName());
		} else {
			pError(player, "There was an issue checking if your world is loaded. Please contact an admin");
		}
	}

	//Sets waitingForConfirm and confirmFor
	public void confirmFor(int id) {
		if(id > 0) {
			confirmFor = id;
			waitingForConfirm = true;
		} else {
			confirmFor = 0;
			waitingForConfirm = false;
		}
	}

	//tp's a player to a specified world's spawn
	public void tpToWorld(Player player, String world) {
		player.teleport(server.getWorld(world).getSpawnLocation());
	}

	//Checks if the world loaded for a specific user
	public void worldLoaded(String worldName) {
		List<World> worlds = server.getWorlds();
		for(World world: worlds) {
			if(world.getName().equalsIgnoreCase(worldName)) {
				worldLoaded = true;
			}
		}
		if(worldLoaded) {
			worldLoadedInt = 0;
			return;
		} else {
			if(worldLoadedInt == 1) {
				adminError("Tried to load a world for " + worldName + " but it was unsuccessful.");
			} else {
				worldLoadedInt++;
				worldLoaded(worldName);
			}
		}
	}

	//BukkitTask for clearing confirmFor
	class RWTask extends BukkitRunnable {
		public RWTask (JavaPlugin plugin) {

		}

		@Override
		public void run() {
			Functions funcs = new Functions();
			funcs.confirmFor(0);

		}
	}
}
